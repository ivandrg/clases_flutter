import 'package:get_storage/get_storage.dart';

class LocalDatasource {
  final namesBox = GetStorage();
  String boxName = 'names';

  getDataFromLocal() {
    final storedMap = namesBox.read(boxName);
    return storedMap ?? '';
  }

  saveDataToLocal(List names) {
    namesBox.write(boxName, names);
  }

  deleteDataToLocal() async {
    // await namesBox.erase();
    namesBox.remove(boxName);
  }
}
