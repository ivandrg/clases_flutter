import 'package:http/http.dart' as http;
import 'package:proyecto_clases/models/api_response_model.dart';

class ApiDatasource {
  final String dom = 'rickandmortyapi.com';
  final String context = 'api/character/';

  Future getDataFromApi(String page) async {
    var url = Uri.https(dom, context, {'page': page});
    final response = await http.get(
      url,
    );
    final ApiResponseModel dataModel = apiResponseModelFromMap(response.body);
    return dataModel;
  }
}
