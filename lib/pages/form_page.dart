import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class FormPage extends StatefulWidget {
  const FormPage({super.key});

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final TextEditingController emailController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  final TextEditingController phoneController = TextEditingController();

  String email = '';
  String password = '';
  String phone = '';

  bool _obscureText = true;

  final _formKey = GlobalKey<FormState>();

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Formularios'),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(20.0),
          children: [
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              controller: emailController,
              decoration: InputDecoration(
                hintText: 'Ingrese su email',
                filled: true,
                fillColor: Colors.black38,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Ingrese un correo';
                } else if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$')
                    .hasMatch(value)) {
                  return 'Ingrese un correo valido';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  email = value;
                });
              },
            ),
            const Gap(20),
            TextFormField(
              keyboardType: TextInputType.visiblePassword,
              obscureText: _obscureText,
              enableSuggestions: false,
              autocorrect: false,
              controller: passwordController,
              decoration: InputDecoration(
                hintText: 'Ingrese su contraseña',
                filled: true,
                fillColor: Colors.black38,
                suffixIcon: GestureDetector(
                  onTap: () {
                    _toggle();
                  },
                  child: const Icon(Icons.remove_red_eye),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Ingrese una contraseña';
                } else if (!RegExp(
                        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~_]).{8,}$')
                    .hasMatch(value)) {
                  return 'Ingrese una contraseña valida';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  password = value;
                });
              },
            ),
            const Gap(20),
            TextFormField(
              keyboardType: TextInputType.phone,
              controller: phoneController,
              decoration: InputDecoration(
                hintText: 'Ingrese su telefono',
                filled: true,
                fillColor: Colors.black38,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.red),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Ingrese un telefono valido';
                } else if (!RegExp(r'^\+?[0-9]{10,12}$').hasMatch(value)) {
                  return 'Ingrese un telefono valido';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  phone = value;
                });
              },
            ),
            const Gap(20),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                          'Ingresaste la siguiente informacion personal'),
                      content: SizedBox(
                        height: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Correo: ${emailController.text}'),
                            Text('Password: ${passwordController.text}'),
                            Text('Telefono: ${phoneController.text}'),
                          ],
                        ),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () {
                            emailController.clear();
                            passwordController.clear();
                            phoneController.clear();
                            Navigator.pop(context, 'Cancel');
                          },
                          child: const Text('Borrar datos'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text('OK'),
                        ),
                      ],
                    ),
                  );
                }
              },
              child: const Text('Guardar'),
            ),
            const Gap(20),
            const Text('Datos Ingresados'),
            ListTile(
              title: const Text('Email'),
              subtitle: Text(email),
            ),
            ListTile(
              title: const Text('Password'),
              subtitle: Text(password),
            ),
            ListTile(
              title: const Text('Telefono'),
              subtitle: Text(phone),
            ),
          ],
        ),
      ),
    );
  }
}
