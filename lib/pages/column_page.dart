import 'package:flutter/material.dart';

class ColumnPage extends StatelessWidget {
  const ColumnPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Column Page'),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Text('data'),
                Container(
                  margin: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.amber,
                      borderRadius: BorderRadius.circular(20),
                      image: const DecorationImage(
                          image: NetworkImage(
                              'https://extension.uned.es/archivos_publicos/webex_actividades/5350/pinaut8bisvg_mini2.jpg'))),
                  width: double.infinity,
                  height: 300,
                  child: const Center(
                    child: Column(
                      children: [
                        Text(
                          'Este es un container widget',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Este es un container widget',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 200,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: Colors.red,
                        child: const Text(
                          'Este es',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        color: Colors.blue,
                        child: const Text(
                          ' un row widget',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.green,
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.cyanAccent,
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.pink,
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.orange,
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  color: Colors.redAccent,
                ),
              ],
            ),
          ),
          const Positioned(
            bottom: 200,
            left: 0,
            child: CircleAvatar(
              radius: 50,
              foregroundImage: NetworkImage(
                  'https://extension.uned.es/archivos_publicos/webex_actividades/5350/pinaut8bisvg_mini2.jpg'),
            ),
          ),
        ],
      ),
    );
  }
}
