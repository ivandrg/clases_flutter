import 'package:flutter/material.dart';
import 'package:proyecto_clases/datasource/api_datasource.dart';

class ApiConnectionPage extends StatefulWidget {
  const ApiConnectionPage({super.key});

  @override
  State<ApiConnectionPage> createState() => _ApiConnectionPageState();
}

class _ApiConnectionPageState extends State<ApiConnectionPage> {
  final api = ApiDatasource();

  int page = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Api Coneccion'),
      ),
      bottomNavigationBar: SizedBox(
        height: 70,
        child: Row(
          children: [
            const Spacer(),
            IconButton(
              onPressed: () {
                setState(() {
                  if (page > 1) {
                    page--;
                  }
                });
              },
              icon: const Icon(Icons.arrow_back),
            ),
            const Spacer(),
            Text(
              page.toString(),
            ),
            const Spacer(),
            IconButton(
              onPressed: () {
                setState(() {
                  page++;
                });
              },
              icon: const Icon(Icons.arrow_forward),
            ),
            const Spacer(),
          ],
        ),
      ),
      body: ListView(
        children: [
          FutureBuilder(
            future: api.getDataFromApi(page.toString()),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data.results.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading:
                          Image.network(snapshot.data.results[index].image),
                      title: Text(
                        snapshot.data.results[index].name,
                      ),
                      subtitle: Text(
                        snapshot.data.results[index].origin.name,
                      ),
                      trailing:
                          Text(snapshot.data.results[index].location.name),
                    );
                  },
                );
              }
              return const SizedBox(
                height: 500,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
