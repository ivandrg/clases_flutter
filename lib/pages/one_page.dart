import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class OnePage extends StatelessWidget {
  const OnePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Pagina 1'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: [
            const Gap(20),
            const Text('Volver a la router page'),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'routesPage', (route) => false);
              },
              child: const Text('Volver a Router Page'),
            ),
            const Gap(40),
            const Text('Volver a la primera ruta'),
            ElevatedButton(
              onPressed: () {
                Navigator.popUntil(context, (route) => route.isFirst);
              },
              child: const Text('Pop'),
            ),
            const Gap(40),
            const Text('Navegar a pagina 1'),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, 'onePage');
              },
              child: const Text('Pagina 1'),
            ),
          ],
        ),
      ),
    );
  }
}
