import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:proyecto_clases/datasource/local_datasource.dart';

class LocalStoragePage extends StatefulWidget {
  const LocalStoragePage({super.key});

  @override
  State<LocalStoragePage> createState() => _LocalStoragePageState();
}

class _LocalStoragePageState extends State<LocalStoragePage> {
  final TextEditingController nameController = TextEditingController();
  List names = [];
  final localData = LocalDatasource();

  @override
  void initState() {
    getDataLocal();
    super.initState();
  }

  getDataLocal() {
    final data = localData.getDataFromLocal();
    if (data.isNotEmpty) names.addAll(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Almacenamiento Local'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        children: [
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            controller: nameController,
            decoration: InputDecoration(
              hintText: 'Ingrese una tarea',
              filled: true,
              fillColor: Colors.black38,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: const BorderSide(color: Colors.red),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: const BorderSide(color: Colors.red),
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              if (nameController.text.isNotEmpty) {
                names.add(nameController.text);
                localData.saveDataToLocal(names);
                nameController.clear();
                setState(() {});
              }
            },
            child: const Text('Guardar Nombre'),
          ),
          const Gap(50),
          ListView.builder(
            shrinkWrap: true,
            itemCount: names.length,
            itemBuilder: (BuildContext context, int index) {
              return Text(
                '${index + 1}. ${names[index]}',
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              );
            },
          ),
          ElevatedButton(
            onPressed: () async {
              if (names.isNotEmpty) {
                localData.saveDataToLocal([]);
                names.clear();
                getDataLocal();
                setState(() {});
              }
            },
            child: const Text('Limpiar lista'),
          ),
        ],
      ),
    );
  }
}
