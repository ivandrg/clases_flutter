import 'package:flutter/material.dart';

enum SingingCharacter { lafayette, jefferson }

class WidgetsUtilesPage extends StatefulWidget {
  const WidgetsUtilesPage({super.key});

  @override
  State<WidgetsUtilesPage> createState() => _WidgetsUtilesPageState();
}

class _WidgetsUtilesPageState extends State<WidgetsUtilesPage> {
  final TextEditingController controller = TextEditingController();
  bool checkbox = false;
  SingingCharacter? _character = SingingCharacter.lafayette;
  double _currentSliderValue = 20;

  int _selectedIndex = 0;

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
  ];

  String selectedPage = '';

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final colorOpt = [Colors.red, Colors.yellow, Colors.blue];

  dynamic colorFondo = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Widgets Utiles'),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Text(
                'Drawer Header',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.message),
              title: const Text('Messages'),
              onTap: () {
                setState(() {
                  selectedPage = 'Messages';
                });
              },
            ),
            ListTile(
              leading: const Icon(Icons.account_circle),
              title: const Text('Profile'),
              onTap: () {
                setState(() {
                  selectedPage = 'Profile';
                });
              },
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Settings'),
              onTap: () {
                setState(() {
                  selectedPage = 'Settings';
                });
              },
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                color: Colors.blue,
                height: 100,
                width: MediaQuery.sizeOf(context).width,
              ),
              Expanded(
                child: Container(
                  color: colorFondo,
                  width: MediaQuery.sizeOf(context).width,
                ),
              ),
              Container(
                color: Colors.grey,
                height: 100,
                width: MediaQuery.sizeOf(context).width,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: ListView(
              children: [
                const Text(
                  'Card',
                  style: TextStyle(fontSize: 15),
                ),
                UnconstrainedBox(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(40),
                      ),
                      child: Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQr6K_-uo13Abm1uswKZm-ma-FleYrc3fs99jYF_M18dO_8Fm3Oaalvr0lHMDCFxV8eNsI&usqp=CAU',
                        height: 200,
                        width: 200,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Text Form',
                  style: TextStyle(fontSize: 15),
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: controller,
                  decoration: InputDecoration(
                    hintText: 'Ingrese su nombre',
                    filled: true,
                    fillColor: Colors.black38,
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'ElevatedButton',
                  style: TextStyle(fontSize: 15),
                ),
                ElevatedButton(
                  onPressed: () {
                    controller.clear();
                  },
                  child: const Text('Borrar'),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'OutlinedButton',
                  style: TextStyle(fontSize: 15),
                ),
                OutlinedButton(
                  onPressed: () {
                    controller.clear();
                  },
                  child: const Text('Borrar'),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Checkbox',
                  style: TextStyle(fontSize: 15),
                ),
                Checkbox(
                  activeColor: Colors.green,
                  shape: const CircleBorder(),
                  side: const BorderSide(color: Colors.red),
                  value: checkbox,
                  onChanged: (value) {
                    setState(() {
                      checkbox = value!;
                    });
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Radio',
                  style: TextStyle(fontSize: 15),
                ),
                ListTile(
                  title: const Text('Lafayette'),
                  leading: Radio<SingingCharacter>(
                    value: SingingCharacter.lafayette,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                ),
                ListTile(
                  title: const Text('Thomas Jefferson'),
                  leading: Radio<SingingCharacter>(
                    value: SingingCharacter.jefferson,
                    groupValue: _character,
                    onChanged: (SingingCharacter? value) {
                      setState(() {
                        _character = value;
                      });
                    },
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Slider',
                  style: TextStyle(fontSize: 15),
                ),
                Slider(
                  value: _currentSliderValue,
                  max: 100,
                  divisions: 5,
                  label: _currentSliderValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue = value;
                    });
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Alert dialog',
                  style: TextStyle(fontSize: 15),
                ),
                TextButton(
                  onPressed: () => showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('AlertDialog Title'),
                      content: const Text('AlertDialog description'),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'OK'),
                          child: const Text('OK'),
                        ),
                      ],
                    ),
                  ),
                  child: const Text('Show Dialog'),
                ),
                const SizedBox(
                  height: 30,
                ),
                const Text(
                  'Bottom Navigation Bars',
                  style: TextStyle(fontSize: 15),
                ),
                _widgetOptions.elementAt(_selectedIndex),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                      ),
                      onPressed: () {
                        setState(() {
                          colorFondo = colorOpt[0];
                        });
                      },
                      child: const Text('Red'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.yellow,
                        foregroundColor: Colors.white,
                      ),
                      onPressed: () {
                        setState(() {
                          colorFondo = colorOpt[1];
                        });
                      },
                      child: const Text('Red'),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        foregroundColor: Colors.white,
                      ),
                      onPressed: () {
                        setState(() {
                          colorFondo = colorOpt[2];
                        });
                      },
                      child: const Text('Red'),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 200,
                ),
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Business',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'School',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
