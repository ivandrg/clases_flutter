import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

class RoutesPage extends StatelessWidget {
  const RoutesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Routes Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
        child: ListView(
          children: [
            const Text('Navigator PushNamed'),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, 'onePage');
              },
              child: const Text('Pagina 1'),
            ),
            const Gap(40),
            const Text('Navigator PushNamedAndRemoveUntil'),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'onePage', (route) => false);
              },
              child: const Text('Pagina 1'),
            ),
            const Gap(40),
            const Text('Navigator pushReplacementNamed'),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacementNamed(context, 'onePage');
              },
              child: const Text('Pagina 1'),
            ),
            const Gap(40),
            const Text('Hacer pop y Navegar a pagina Column Page'),
            ElevatedButton(
              onPressed: () {
                Navigator.popAndPushNamed(context, 'columnPage');
              },
              child: const Text('Pagina 1'),
            ),
          ],
        ),
      ),
    );
  }
}
