import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: const Text(
              'Pagina con Column, Row, Stack',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'columnPage');
            },
          ),
          ListTile(
            title: const Text(
              'Rutas',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'routesPage');
            },
          ),
          ListTile(
            title: const Text(
              'Ejemplos de posicionamiento con Stack',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'stackPage');
            },
          ),
          ListTile(
            title: const Text(
              'Widgets Utiles',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'widgetsPage');
            },
          ),
          ListTile(
            title: const Text(
              'Formularios',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'formPage');
            },
          ),
          ListTile(
            title: const Text(
              'Conexion API',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'apiPage');
            },
          ),
          ListTile(
            title: const Text(
              'Almacenamiento Local',
            ),
            trailing: const Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.pushNamed(context, 'localStoragePage');
            },
          ),
        ],
      ),
    );
  }
}
