import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:proyecto_clases/pages/api_conexion_page.dart';
import 'package:proyecto_clases/pages/column_page.dart';
import 'package:proyecto_clases/pages/form_page.dart';
import 'package:proyecto_clases/pages/home_page.dart';
import 'package:proyecto_clases/pages/local_storage_page.dart';
import 'package:proyecto_clases/pages/one_page.dart';
import 'package:proyecto_clases/pages/routes_page.dart';
import 'package:proyecto_clases/pages/stack_page.dart';
import 'package:proyecto_clases/pages/widgets_utiles_page.dart';

void main() async {
  await GetStorage.init();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: routes(),
      initialRoute: '/',
    );
  }

  Map<String, WidgetBuilder> routes() {
    return <String, WidgetBuilder>{
      '/': (BuildContext context) => const HomePage(),
      'routesPage': (BuildContext context) => const RoutesPage(),
      'onePage': (BuildContext context) => const OnePage(),
      'columnPage': (BuildContext context) => const ColumnPage(),
      'stackPage': (BuildContext context) => const StackExamplesPage(),
      'widgetsPage': (BuildContext context) => const WidgetsUtilesPage(),
      'formPage': (BuildContext context) => const FormPage(),
      'apiPage': (BuildContext context) => const ApiConnectionPage(),
      'localStoragePage': (BuildContext context) => LocalStoragePage(),
    };
  }
}
